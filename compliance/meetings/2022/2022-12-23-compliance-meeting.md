# 合规SIG例会 2022-12-23 15:30-16:30(UTC+08:00)Beijing

## 议题Agenda
- BK7231M gcctoolchain使用合规评审  金练
- KHDVK-450A开发板GCC使用评审    王成


## 与会人Attendees
- 陈雅旬、高琨、郑志鹏、丛林、余甜、方晓、周乃鑫、高亮、董金光、金练、涂文星、王成、王忠进、缪嘉男、刘德帅

## 纪要Notes
- 议题1、BK7231M gcc toolchain使用合规评审

会议内容

  1． libgcc.a为静态引用，使用GPLV3 license，其中license附加GCC Runtime Library Exception条款。满足条例中的例外情况。

  2． libg.a 使用BSD3 License

  3． libnosys.a不再使用，不涉及后续合规问题

4．libc.a libm.a用平台提供的libmusl-c.a和libmusl-m.a代替，没有风险

会议结论

1． 从开源合规法务维度评审，仅针对本次开发板对于GCC工具链的使用方式（如会议内容所示）满足法务合规要求，符合GPLv3例外条件

2． 由于GCC相关GPLv3风险在不同使用方式下会产生风险，尽管本次使用合规，但仍有可能将风险进一步遗留给下游，因此，需由合规SIG对于gcc风险管理的业界最佳实践和后续社区如何管理建议进行进一步调研，并给出建议。

3． 由于项目进度要求，同意在业界实践调研和管理建议完成前，可同步进行其他评审。

- 议题2、KHDVK-450A开发板GCC使用评审

会议内容
1.开发板使用的编译工具为:gcc version 10.3.1，下载地址为https://developer.arm.com/downloads/-/gnu-rm;

2.开发板共使用了libstdc++.a、libsupc++.a、libgcc.a、libm.a、libc.a五个库，均采用静态链接的方式;

3.libstdc++.a、libsupc++.a、libgcc.a等库使用了GPL3.0 license，其中license附加GCC Runtime Library Exception条款。

4.libm.a和libc.a使用了LGPL license和BSD license

会议结论

1．  从开源合规法务维度评审，仅针对本次开发板对于GCC工具链的使用方式（如会议内容所示）满足法务合规要求，符合GPLv3例外条件。

2．  由于GCC相关GPLv3风险在不同使用方式下会产生风险，尽管本次使用合规，但仍有可能将风险进一步遗留给下游，因此，需由合规SIG对于gcc风险管理的业界最佳实践和后续社区如何管理建议进行进一步调研，并给出建议。

3．  由于项目进度要求，同意在业界实践调研和管理建议完成前，可同步进行其他评审。


