#  Augest 6, 2021 at 10:00am-12:00am GMT+8

## Agenda

- OH Dev-Board SIG例会[会议录制链接](https://meeting.tencent.com/user-center/shared-record-info?id=e31e853d-6f9c-40bd-ae5d-94802c757f2c&from=6)

  |    时间     | 议题                                               | 发言人                                                       |
  | :---------: | -------------------------------------------------- | ------------------------------------------------------------ |
  | 10:00-10:20 | OH富设备核心板接口规范确认                         | 全体参会人员                                                 |
  | 10:20-10:40 | 成员单位芯片以及开发板适配进展、兼容性认证进展更新 | 发言顺序：朗国-颜专、博流-韩锐、瑞芯微-彭华成、润和-石磊、博通-王学军、全志- 刘明缘、小熊派-王城、君正-洪涛 |
  | 10:40-11:00 | DAYU平台小系统调试经验分享                         | 润和-杨红亮                                                  |
  | 11:00-11:20 | OH富设备开发板介绍                                 | 刘洋                                                         |
  | 11:20-11:40 | 新进成员单位介绍：新大陆、海通高科                 | 新大陆-许健、海通高科-罗传藻                                 |
  | 11:40-12:00 | SIG成员单位的分工                                  | 刘洋、黎高鹏、李凯                                           |

## Attendees

- [@liuyang198591](liu_yang@hoperun.com)
- [@Laowang-BearPi](wangcheng@holdiot.com)
- [@zhao_xiuxiu ](1605427044@qq.com)
- [@kevenNO1 ](likai20@iscas.ac.cn)
- [@jony_code ](longjun@iscas.ac.cn)
- [@minglonghuang ](minglong@iscas.ac.cn)

## Notes

- 议题一、OH富设备接口规范确认
  为了减少硬件相关碎片化的问题， 针对OpenHarmony设备，制定接口层的规范；该规范[《【OH-Board-SIG】开源板富设备核心板规范_V0.3-20210805》](https://gitee.com/openharmony-sig/sig-content/blob/master/devboard/support%20doc/%E3%80%90OH-Board-SIG%E3%80%91%E5%BC%80%E6%BA%90%E6%9D%BF%E5%AF%8C%E8%AE%BE%E5%A4%87%E6%A0%B8%E5%BF%83%E6%9D%BF%E8%A7%84%E8%8C%83_V0.3-20210805.docx)针对OpenHarmony 6个级别的设备，进行了三个分类（L0，L1-L2，L3-L5)。目前是L3-L5 相关的接口规范，该规范开放的标准：主要面向有兴趣开发鸿蒙开源板兼容产品或服务的芯片供应商和用户，以及有意加入鸿蒙开源生态系统的品牌厂商,一起来构建开放的鸿蒙开源板规范化行业标准， 打造一个全新进化的鸿蒙生态系统。该规范是完全可以自由实施的——即任何人都可以按照该规范建立一个标准化组织，而无需支付任何费用或任何许可要求，该规范已经在SIG内公布一个月，会议上各单位能对该规范进行表决，会议表决情况统计如下： 参与表决单位共8家，其中8家单位表决通过（百问网  、华为、 朗国、基金会、 博通、九联、小熊派、 东软），该规范表决通过。
  
-  议题二、各单位开发板适配进展与兼容性认证进展详情

  各单位开发板适配进展与兼容性认证进展详见石墨文档[Dev-Board开发板列表](https://shimo.im/sheets/VMAPV7xgK2UZPGqg/MODOC)，涉及的[兼容性认证指南](https://www.openharmony.cn/old/#/guide)请在基金会官网查阅，如果对兼容性认证有疑问请联系基金会房隆军、梁克雷

- 议题三：DAYU平台小系统调试经验
  详情请查阅文档[ DAYU 平台OpenHarmony 适配指导 ](https://gitee.com/openharmony-sig/devboard_device_hihope_build)
  
- 议题四：OH富设备开发板介绍

  润和DAYU开发板、中软院软件所树莓派开发板、九联人工智能L2开发板、博流BL602 L0 开发板、上海博通BK7231M L0开发板、海思3516D L2开发板功能特性介绍，详情请查阅文档[OH富设备开发板介绍](https://gitee.com/openharmony-sig/sig-content/blob/master/devboard/support%20doc/%E6%B5%B7%E9%80%9A%E9%AB%98%E7%A7%91OH%E5%BC%80%E5%8F%91%E4%BB%8B%E7%BB%8D.pdf)

- 议题五：新进成员单位介绍

  [新大陆介绍文档](https://gitee.com/openharmony-sig/sig-content/blob/master/devboard/support%20doc/%E6%96%B0%E5%A4%A7%E9%99%86%E8%87%AA%E5%8A%A8%E8%AF%86%E5%88%AB%E5%85%AC%E5%8F%B8%E7%AE%80%E4%BB%8B.pptx)、[海科高通介绍文档](https://gitee.com/openharmony-sig/sig-content/blob/master/devboard/support%20doc/%E6%B5%B7%E9%80%9A%E9%AB%98%E7%A7%91OH%E5%BC%80%E5%8F%91%E4%BB%8B%E7%BB%8D.pdf)

- 议题六： SIG成员单位分工

  1、各家芯片公司需要有单位去主导对接，这样更具针对性，能更快的完善OH生态。

  2、目前需要给外部开发者提供一个官方购买开发板的统一入口，将开发板尽快的推广出去

  3、开发板的认证，基金会层面目前主要是XPS偏软件层面的认证，后续会逐步加入一些硬件层面的认证，以确保开发板的质量。

  ## Action items

  1、各家芯片公司需要有单位去主导对接，如何进行分工---刘洋、黎高鹏、李凯

  2、提供给外部开发者OH开发板购买的官方入口---刘洋、黎高鹏、李凯

  

  