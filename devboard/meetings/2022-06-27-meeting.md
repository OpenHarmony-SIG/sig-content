#  Jun 27, 2022 at 15:00pm-16:00pm GMT+8

## Agenda

- OH Dev-Board SIG例会

  录制文件：https://meeting.tencent.com/v2/cloud-record/share?id=5d60747f-74e2-40d1-999a-32169b173887&from=3

- | 时间        | 议题                               | 发言人              |
  | ----------- | ---------------------------------- | ------------------- |
  | 15:00-15:20 | 各单位开发板状态更新               | SIG工作组-赵秀秀    |
  | 15:20-15:40 | 《开源板瘦设备核心板规范》意见征集 | 规范编写小组-连志安 |
  | 15:40-15:50 | 7月底 基金会年会开发板展示征集     | 营销小组-林丹妮     |
  | 15:50-16:00 | 开放讨论                           | 全体                |
  
- [@liuyang198591](liu_yang@hoperun.com)

- [@Laowang-BearPi](wangcheng@holdiot.com)

- [@zhao_xiuxiu ](1605427044@qq.com)

- [@kevenNO1 ](likai20@iscas.ac.cn)

- [@jony_code ](longjun@iscas.ac.cn)

- [@minglonghuang ](minglong@iscas.ac.cn)

## Notes

- 议题一、各单位开发板状态更新
  本周期内新增3块开发板通过兼容性认证，分别是 九联/Unionpi Tiger晶晨A311D 、润和 Hi3516 AI Camera 、 深开鸿KHDVK-3568A 开发板 ，部分适配中的开发板因为人力不足，当前进度停滞、全志XR872、D1以及宸松教育板。 深开鸿KHDVK-3566B 开发板 、 鸿元智通 HM-3568工业级鸿蒙核心板 、 扬帆系列“竞”开发板、  泰凌微电子  TLSR9系列 目前已经提交兼容性认证，等待结果中。当前开发板进展详情请阅【腾讯文档】DevBoard-SIG开发板列表
  https://docs.qq.com/sheet/DYmZ1RmhEZ1RVa0to
  
- 议题二、 《开源板瘦设备核心板规范》意见征集

  ，会上针对智能模组SOC的内存规格重新讨论并输出新版 瘦设备核心板规范
  
- 议题三：7月底 基金会年会开发板展示征集

  由于本次基金会年会规模较大，且报名时间截止时间为6月30号，时间紧急，营销小组默认当前 Dev-Board SIG 看护的所有开发板全部参加展览，待基金会审核后确认最终的名单。

## Action items

1、《开源板瘦设备核心板规范》V0.2版本审核工作组评审---赵秀秀

2、基金会年会参展开发板持续跟进---赵秀秀



  

  

  