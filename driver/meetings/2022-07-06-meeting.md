#  July 06, 2022 at 16:00am-17:30am GMT+8

## Agenda

- OH Driver SIG例会[](https://meeting.tencent.com/s/O6GmTrcZcV8a)

  |    时间     | 议题                                 | 发言人                                             |
  | :---------: | ------------------------------------ | ------------------------------------------------- |
  | 16:00-16:10 | 议题一、Media的codec驱动能力开发进展 | 张国荣，翟海鹏，刘飞虎 |
  | 16:10-16:25 | 议题二、OpenHarmony设备接口设计规范解读 | 赵文华，刘飞虎，袁博，翟海鹏，刘洪刚 |
  | 16:25-16:40 | 议题三、wlan模块HDI新增数据类型评审 | 赵文华，刘飞虎，袁博，翟海鹏，徐赛，冯永辉，刘洪刚，余甜，黄一宏 |

## Notes

- **议题一、Media的codec驱动能力开发进展----张国荣，翟海鹏，刘飞虎**

  议题进展：Codec HDI1.0实现根据新的头文件适配，支撑2.0转1.0需求联调，并处理部分1.0遗留问题；Codec HDI2.0 Gstream正常流程已完成对接调试，Histream对接调试中；遗留问题跟踪闭环。

- **议题二、OpenHarmony设备接口设计规范解读----赵文华，刘飞虎，袁博，翟海鹏，刘洪刚**

  议题结论：按照OpenHarmony项目PMC评审结论，设计OpenHarmony设备接口设计规范Beta试运行版，解读了规范设计的关键规则和建议。在试运行1个月，试运行解读欢迎各位开发者通过邮件列表或者仓下issue提出交流意见， 

  [规范归档路径：https://gitee.com/openharmony/docs/blob/master/zh-cn/design/hdi-desi…](http://xn--:https-2y2l2mi59dn14dnsp6yl//gitee.com/openharmony/docs/blob/master/zh-cn/design/hdi-design-specifications.md)

- **议题三、wlan模块HDI新增数据类型评审----赵文华，刘飞虎，袁博，翟海鹏，徐赛，冯永辉，刘洪刚，余甜，黄一宏**

  议题结论：对wlan模块新增HDI接口和数据类型进行评审，新增4个hml和投屏优化HDI接口，接口定义符合合规、兼容性和扩展性要求，接口评审通过
  
  HDI接口列表：
  
  | 接口                                                         | 评审结论 |
  | ------------------------------------------------------------ | -------- |
  | NotifyMessage([in] String ifName, [in] struct HmlEventParam data) | 通过     |
  | GetCoexChannelList([in] String ifName, [out] unsigned char[] buf) | 通过     |
  | SendHmlCmd([in] String ifName, [in] struct HmlCmdParam param) | 通过     |
  | SetProjectionScreenParam([in] String ifName, [in] struct ProjectionScreenCmdParam data) | 通过     |
  
  遗留问题： 
  
  1. commandId起始值难以理解，且数字不连续。改为枚举型定义 。
  2. buf数据缺少详细描述。增加buf数据定义详细描述。
  3. 法务部余甜评审接口。已拉余甜评审并通过。

 会议通知：(https://lists.openatom.io/hyperkitty/list/sig_driver@openharmony.io/thread/LRUF7EH4VDR5BK5G46U5JBQS2ILCQONY/)

 会议议题申报：https://shimo.im/sheets/36GKhpvrXd8TcQHY/AdiEd

 会议归档：https://gitee.com/openharmony-sig/sig-content/tree/master/driver/meetings


  ## Action items
