#  August 31, 2022 at 16:00am-17:30am GMT+8

## Agenda

- OH Driver SIG例会[](https://meeting.tencent.com/s/O6GmTrcZcV8a)

  |    时间     | 议题                                 | 发言人                                             |
  | :---------: | ------------------------------------ | ------------------------------------------------- |
  | 16:00-16:10 | 议题一、Media的codec驱动能力开发进展 | 张国荣，张国荣，翟海鹏，刘飞虎，孙赫赫 |
  | 16:10-16:25 | 议题二、Display模块新增结构体属性值对应枚举定义评审 | 赵文华，刘飞虎，袁博，翟海鹏，刘洪刚，黄一宏 |
  | 16:25-16:40 | 议题三、USB模块变更HDI接口评审 | 赵文华，刘飞虎，袁博，刘飞虎，吴成文，刘洪刚，黄一宏 |

## Notes

- **议题一、Media的codec驱动能力开发进展----张国荣，翟海鹏，刘飞虎，孙赫赫**

  议题进展：Codec HDI1.0 非自动分帧解码流程已合入，多任务并发的功能已完成，编解码流程正常；Codec HDI2.0 idl化改造开发完成，在提交ild分支中，支撑媒体服务联调。

- **议题二、Display模块新增结构体属性值对应枚举定义评审----赵文华，刘飞虎，袁博，翟海鹏，刘洪刚，黄一宏**

  议题结论：对Display模块新增数据类型进行评审，新增1个结构体AllocInfo字段usage，接口定义符合合规、兼容性和扩展性要求，接口评审通过。

- **议题三、USB模块变更HDI接口评审----赵文华，刘飞虎，袁博，刘飞虎，吴成文，刘洪刚，黄一宏**

  议题结论：对usb模块变更HDI接口进行评审，不同意HDI接口新增驱动的DFX信息获取接口，DFX的能力有USB驱动单独完成，不对外提供接口能力，接口变更评审不通过。

 会议通知：(https://lists.openatom.io/hyperkitty/list/sig_driver@openharmony.io/thread/7TC3STRU7WUF6I4JZMOQVB2GVBYQCYPD/)

 会议议题申报：https://shimo.im/sheets/36GKhpvrXd8TcQHY/AdiEd

 会议归档：https://gitee.com/openharmony-sig/sig-content/tree/master/driver/meetings


  ## Action items
