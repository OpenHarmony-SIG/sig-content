# May 19, 2023 at 17:00pm GMT+8

## Agenda
- OH4.1图形规划研讨
- 图形Review规则分工讨论

## Attendees
- abbuu(https://gitee.com/abbuu)
- zhouyaoying(https://gitee.com/zhouyaoying)
- lijj01(https://gitee.com/lijj01)
- lz-230(https://gitee.com/lz-230)
- bj1010(https://gitee.com/bj1010)
- mk_gc(https://gitee.com/mk_gc)
- helloGraphics(https://gitee.com/helloGraphics

## Notes

会议纪要：

议题一：OH 4.1 图形规划研讨
议题结论：
- 完成4.1的需求规划同步
- 将4.1规划同步归档到图形sig社区

议题二：图形Review 规则分工讨论
议题结论：
- 达成图形Review 规则分工
- 将Review规则同步图形Sig社区

## Action items
- NA
