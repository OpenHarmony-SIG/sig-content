#  September 30, 2021 at 14:30am-15:50am GMT+8

##  Agenda

- [Industrial_internet_sig](https://gitee.com/openharmony/community/tree/master/sig/sig-industrial_internet)（工业互联网SIG）例会[会议录制链接](https://meeting.tencent.com/user-center/shared-record-info?id=a3ce7cb9-8bc8-4147-b440-39e68892d1ce&from=6)

  |    时间     | 议题                                           | 发言人   |
  | :---------: | ---------------------------------------------- | -------- |
  | 14:30-14:40 | 整体情况介绍                                   | 黄斌     |
  | 14:40-15:20 | 物模型结构、通讯协议规范设计说明与评审         | 陈建华   |
  | 15:20-15:40 | 工业物联网数据库Apache IoTDB基于OH推进思路分享 | 乔嘉林   |
  | 15:40-15:50 | 自由讨论                                       | 全体成员 |

##  Attendees

- [@黄斌（halhuangbin）](https://gitee.com/halhuangbin)
- [@陈建华（gavinChen）](https://gitee.com/gavin1234)
- [@吴俊昭（woohey）](https://gitee.com/woohey)
- [@谢石河（xieshihe）](https://gitee.com/xieshihe)
- 黄斌、吴俊昭、陈建华、乔嘉林、李小鹏、戴东东、刘宏俊、曾志鹏、孟智山、尹健、佘鑫辉、刘助政、周关益、喻海军、李钊、刘大伟、候昊男、房隆俊、天空、林范贵、彭先林、张勇、曾智广

##  Notes

- 议题一、[工业互联网SIG整体情况介绍](../docs/工业互联网SIG双周会议20210930.pdf)
  
  1、创始团成员单位介绍
  
  2、工业物联网平台架构介绍
  
  3、上两周工作总结
  
  1）工业设备物联网结构设计
  
  2）网关、设备、IoT通讯协议格式定义
  
  3）工业设备接入SDK设计
  
  4）Apache IoTDB基于Openharmony的推进思路和计划

- 议题二、物模型结构、通讯协议规范设计说明与评审

  1、拓维工业物联网平台架构概述介绍

  1）开发统一的设备接入SDK（方便设备接入）

  2）制定设备与物联网平台同学协议规范（规范数据传输）

  3）定义统一的物模型结构（规范设备开发与应用开发）

  2、物模型结构、通讯协议设计

  1）[IoT物联网平台物模型结构定义（属性、命令、事件）](../docs/工业物联网-物模型设计文档.md)

  2）[工业物联通讯网协议格式定义](../docs/工业物联网-协议规范设计文档.md)

  3）[工业设备接入SDK设计介绍](../docs/工业物联网-设备接入SDK设计文档.md)

- 议题三：[工业物联网数据库Apache IoTDB基于OH推进思路分享](../docs/工业物联网数据库Apache IoTDB基于OH推进思路分享.pdf)

  1、IoTDB组件介绍

  2、端、边、云产品形态介绍

  3、时序数据生命周期介绍（采集--管理--处理--分析）

  4、后续计划

  1）C语言客户端验证（已有，需在Openharmony验证）

  2）C版本时序文件格式TsFile（2022-06-01）

  3）C版本同步机制（2022-12-31）

  4）C版本数据库引擎（2022-12-31）

  5、应用场景：端侧数据文件管理、C版本同步工具

- 议题四：自由讨论

  1、仙翁科技公司及产品介绍（戴东东）

  ##  Action items

  1、《工业物联网协议规范设计文档》上传到社区

  2、下周计划

  1）基于OH开发的SDK代码评审和案例讲解

  2）Apache IoTDB C语言客户端适配OH案例讲解

  3）基于OH的边缘安全体系开源推进思路