# May.17, 2022 at 10:00am GMT+8

## Agenda
|时间|议题|发言人|
|--|--|--|
|10:00-10:15|Mips第一阶段已完成任务分享|袁祥仁 王少峰 李兵|
|10:15-10:25|Mips第二阶段任务对齐|袁祥仁 王少峰 王兴 李兵|
|10:25-10:40|工程编译框架适配进展分享|Lain 王少峰 王兴 李兵|

## Attendees
- [@hongtao6573](https://gitee.com/hongtao6573)
- [@cip_syq](yunqiang.su@oss.cipunited.com)
- [@Lain]()
- [@wicom](https://gitee.com/wicom)
- [@xiongtao](https://gitee.com/xiongtao)
- [@wangxing-hw](https://gitee.com/wangxing-hw)
- [@libing23](https://gitee.com/libing23)
- [@huangsox](https://gitee.com/huangsox)
- [@liujk000](https://gitee.com/liujk000)


## Notes

#### 议题一、Mips第一阶段已完成任务分享

**详情**
- 组件适配master分支任务本地适配完成，但直接在系统master分支上编译有异常，需要适配编译框架。

**结论**
- master分支下编译框架适配工作和Lain配合推进。
- ai模块组件有平台无关改动，可提起pr。
- graphic组件有mips平台相关改动，需要系统编译框架适配完成后，验证效果，暂时不提起pr。


#### 议题二、Mips第二阶段任务对齐

**结论**
- 媒体子系统中驱动代码上主干需要是HDF驱动形式。


#### 议题三、工程编译框架适配进展分享

**详情**
- 当前编译框架中部分地方写死了arm，正在修改为mips。
- 三方系统组件适配正在分析。

**结论**
- 三方系统组件请Lain先按以下情况标记区分，之后社区确定适配工作安排。
    1. 是否有汇编需要适配
    2. 未适配情况下组件无法工作还是性能有损失
- Lain和君正合作解决master分支下编译的问题，OH3.0下的编译框架适配任务停止。
- 通过其他途径为mips sig增加人力资源，帮助Lain完成工程编译适配任务。 -责任人黄首西

## Action items
