# August.16, 2022 at 10:00am GMT+8

## Agenda
|时间|议题|发言人|
|--|--|--|
|10:00-10:15|Mips编译工具和框架适配进展同步|袁祥仁 王劭锟 李兵|
|10:15-10:25|子系统适配进展同步|袁祥仁 黄首西|
|10:25-10:35||系统三方组件适配|Lain 黄首西 刘佳科|

## Attendees
- [@hongtao6573](https://gitee.com/hongtao6573)
- [@cip_syq](yunqiang.su@oss.cipunited.com)
- [@Lain]()
- [@wicom](https://gitee.com/wicom)
- [@wangxing-hw](https://gitee.com/wangxing-hw)
- [@libing23](https://gitee.com/libing23)
- [@huanghuijin](https://gitee.com/huanghuijin)
- [@huangsox](https://gitee.com/huangsox)
- [@liujk000](https://gitee.com/liujk000)


## Notes

#### 议题一、Mips编译工具和框架适配进展同步

**结论**
- 1.编译工具链的pr已经提交，正在等待仓库committer审核。
- 2.编译框架修改后可以跑完编译流程，计划在8月底前添加必要子系统，并在硬件上验证效果。责任人：袁祥仁 王劭锟

#### 议题二、子系统适配进展同步

**结论**
- 君正在L1系统的图形，多媒体等子系统中正在参与完善功能的需求，8月底前同步出相关计划。

#### 议题三、系统三方组件适配进展同步

**结论**
- 1.flutter框架由Dart语言编写，目前Dart语言对mips平台没有支持，正在联系flutter官方了解适配可行性。
- 2.对于ssl相关库，适配需要的msa功能文档缺少，芯联芯正在联系合作方提供，预计9月底前完成。
- 3.ffmpeg相关库已有msa功能适配，需要在硬件上验证。

## Action items
