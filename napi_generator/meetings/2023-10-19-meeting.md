## Agenda

- OH 辅助工具 SIG例会

  |    时间     | 议题                                                         | 发言人                 |
  | :---------: | ------------------------------------------------------------ | ---------------------- |
  | 10:00-11:00 | napi_generator工具适配demo进展 | 苏重威，赵军霞，苟晶晶 |

## Attendees

- [@zhaojunxia2020](https://gitee.com/zhaojunxia2020)
- [@gou-jingjing](https://gitee.com/gou-jingjing)
- [@su-chongwei](https://gitee.com/su-chongwei)

## Notes

- 会议内容：

  1.汇报工具适配进展：适配中需求：支持 JS 对象访问及操作、支持主线程 JS 函数回调；

  2.澄清napi工具回调处理逻辑：包括AsyncCallback、Callback、on(事件固定取值、非固定取值)；

  3.讲解工具如何实现从js层到C++层的调用；

  4.提出当前napi工具需求的歧义，待澄清

  ## Action items

  1、异步带返回值的回调暂时不支持，需确认当前是否支持，以及方案如何；//已确认暂时不支持，苏重威，2023.10.25

  2、对于不希望触发的回调，如何定义语法；//赵军霞 定义此场景工具使用语法

