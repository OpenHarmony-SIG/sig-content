## Agenda

- OH 辅助工具 SIG沟通会

  |    时间     | 议题                       | 发言人       |
  | :---------: | -------------------------- | ------------ |
  | 11:00-12:00 | napi_generator美团问题情况对齐 | 龚俊松，刘鑫，赵军霞，巴延兴 |

## Attendees

- [@zhaojunxia2020](https://gitee.com/zhaojunxia2020)
- [xliu-huanwei](https://gitee.com/xliu-huanwei)
- 龚俊松
- 巴延兴

## Notes

- 会议内容：

  1. 澄清napi当前特性能力，澄清callback的含义非泛值等
  2. 讨论美团demo工具当前实现方式的合理性

  ## Action items

  1、增加用例与特性对应关系  -- 赵军霞

  2、梳理特性的限制约束 -- 赵军霞

  3、对工具新版本进行测试，提出优化需求  -- 刘鑫
  
  